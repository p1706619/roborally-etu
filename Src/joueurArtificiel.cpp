#include "joueurArtificiel.hpp"
#include <random>
#include <vector>
#include <queue>

using namespace std;

// permet d'initialiser le tableau de mouvements pris au hasard
void JoueurA::init_tab_actions(){ 
    random_device rd;  
    mt19937 gen(rd()); 
    uniform_int_distribution<> distrib(0, 6); //uniform_int_distribution permet d'obtenir 
    for (int i=0; i<9; ++i)                   //une distribution plus homogène entre deux entiers  
    {                                         //que la fonction rand()%
        actions[i]=distrib(gen);  
    }

    dernierePosition=0; //on initialise au passage l'indice qui permettera de savoir
                        //où on peut commencer à piocher, ici 0
}

// permet d'afficher le tableau de mouvements //
void JoueurA::afficher_tab_actions(){
     for (int i=0; i<9; ++i)
    {
        cout << actions[i]<<" ";
    }
    cout<<endl;

}

// permet d'echanger de place les mouvements 
void JoueurA::echange(int m){
    

    for(int i=dernierePosition; i<9; i++){
        if(m==actions[i]){
            //Echange
	        int nbTire=actions[dernierePosition];
	        actions[dernierePosition]=actions[i];
	        actions[i]=nbTire;
            
            dernierePosition++;  // on maintenant un mouvement que le joueur peut joueur, à la position dernierePosition
                                // la prochaine fois qu'on pioche, on le fera entre dernierePosition et 9
            break;                    
        }
    }    
}

// calcul du chemin, avec adaptation de l'algorithme de Dijkstra à notre problème //
void JoueurA::calcul_chemin(Graphe & g,int d,int a){
   
    Sommet * arrive=&g.sommets[a];
    Sommet * depart=&g.sommets[d];
    queue<Sommet*> aTraiter;
    
    for(std::size_t  i=0; i< g.sommets.size(); i++){
        g.sommets[i].distance=g.sommets.size();
        g.sommets[i].precedent=&(g.sommets[i]);

        for(int j=0; j<7; j++){
           g.sommets[i].voisins[j].poids=1;
           g.sommets[i].mouvements.push_back(g.sommets[i].voisins[j].move);
          
        } 
    }

    depart->distance=0;
    depart->precedent=nullptr;
    aTraiter.push(depart);
    Sommet * aVisiter;

    while(!aTraiter.empty() && (aVisiter!=arrive || dernierePosition<6)){
        aVisiter = aTraiter.front(); // on récupère un sommet à traiter  
         for(int i=dernierePosition; i<9; i++){     //taille du tableau    
             if(aVisiter->voisins[actions[i]].sommetA->distance > aVisiter->distance + aVisiter->voisins[actions[i]].poids ) // +1 car ici les arêtes ont un poid de 1
             {
                  aVisiter->voisins[actions[i]].sommetA->distance=aVisiter->distance+aVisiter->voisins[actions[i]].poids;
                  aVisiter->voisins[actions[i]].sommetA->precedent=aVisiter;  
                 
                  joueur=aVisiter->voisins[actions[i]].sommetA;
                    echange(actions[i]);
                  aTraiter.push(aVisiter->voisins[actions[i]].sommetA);  
                              
             }

           }
         aTraiter.pop();  //enlever le premier élément qui est aVisite  

    }
    
}

//permet d'afficher le parcours//
void JoueurA::afficher_parcours(Graphe * g,int depart,int arrive){

    cout<<"Sommet de Depart :"<< g->sommets[depart].depart.location.line <<" "<<
                                     g->sommets[depart].depart.location.column <<" "<<
                                    (int)g->sommets[depart].depart.status<<endl;

    cout<<"Sommet que l'on cherche à atteindre :"<< g->sommets[arrive].depart.location.line <<" "<<
                                     g->sommets[arrive].depart.location.column <<" "<<
                                     (int)g->sommets[arrive].depart.status<<endl;


   cout<<"Sommet d'arrive du joueur artificiel :"<<joueur->depart.location.line<<" "<<
                                                joueur->depart.location.column<<" "<<
                                                (int)joueur->depart.status<<endl;
    
    Sommet * a=&(g->sommets[arrive]);
    for(int i=0; i<5; i++){
       if(joueur->egal_Robot_Sommet(a)) break;
       else{
        joueur->afficher_mouvement(actions[i]);
        joueur->afficher_direction();
        }
    }

}
