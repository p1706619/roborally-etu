#include "graphe.hpp"
#include <queue>


// Fonctions et Procedure de la structure Graphe //

// Procedure d'initialisation du graphe //
void Graphe::init_graphe(Board &board){

    Sommet mort;
    mort.depart.status = Robot::Status::DEAD;
    sommets.push_back(mort);
    
    for(std::pair<Location, Board::TileType> tile : board.tiles) { // pour chaque case du tableau
    
        for(int i=0; i <= 3;i++){                      // pour chaque orientation 
        Sommet initial;

        initial.depart.location.line = tile.first.line;
        initial.depart.location.column = tile.first.column;
        initial.depart.status = (Robot::Status)i;    // i=0 EAST, i=1 NORTH
                                                    // i=2 WEST, i=3 SOUTH
        sommets.push_back(initial);
        }               
    }    
    for(std::size_t  i=0; i< sommets.size(); i++){  // on relie les sommets entre eux 
        for(int j=0; j<7; j++) { //pour chaque coup possibles
        Robot jouer=sommets[i].depart; 
        board.play(jouer, (Robot::Move)j); //  i=0 FORWARD_1
                       
            if(jouer.status==Robot::Status::DEAD){ //si le robot est mort       
            sommets[i].voisins[j].sommetA=&(sommets[0]);
            sommets[i].voisins[j].move=j;            
             }
            else{
                for(std::size_t  k=1; k<sommets.size(); k++){ //relier le robot à un sommet du graphe
                     if(jouer.location==sommets[k].depart.location && jouer.status==sommets[k].depart.status)
                    {
                    sommets[i].voisins[j].sommetA=&(sommets[k]); 
                    sommets[i].voisins[j].move=j;
                     break;
                     }
                }
            }
        }
    }
} 
// Parcours en largeur du graphe //
void Graphe::parcourir_graphe(int depart)
{    
    queue<Sommet*> aTraiter;
   for(std::size_t  i=0; i< sommets.size(); i++){
           sommets[i].visite=false;
           sommets[i].distance=sommets.size();
           for(int j=0; j<7; j++){
               sommets[i].voisins[j].poids=1;
               sommets[i].mouvements.push_back(sommets[i].voisins[j].move);
           }          
    }
    sommets[depart].precedent=nullptr;
    sommets[depart].visite=true; // on le marque comme visité
    sommets[depart].distance=0;
    aTraiter.push(&sommets[depart]); // on ajoute le sommet de départ à traiter
 
    while(!aTraiter.empty()){      //tant qu'il y a des sommets à traiter
        Sommet * aVisiter = aTraiter.front(); // on récupère un sommet à traiter
         for(int i=0; i<7; i++){    // un sommet a au plus 7 voisins
            if(!aVisiter->voisins[i].sommetA->visite){                
                aVisiter->voisins[i].sommetA->visite=true;  // on marque le voisin comme vu
                aVisiter->voisins[i].sommetA->distance = aVisiter->distance + aVisiter->voisins[i].poids; 
                aVisiter->voisins[i].sommetA->precedent=aVisiter;           
                aTraiter.push(aVisiter->voisins[i].sommetA); //on l'ajoute aux sommets à traiter                               
            }                 
        }
        aTraiter.pop();  //enlever le premier élément qui est aVisite  
    }      
}

// Verfication du parcour en largeur //
void Graphe::verif_parcours()
{
    for(std::size_t  i=0; i< sommets.size(); i++){
        if(sommets[i].visite==false) cout<<"On a manqué un sommet "<< i <<endl;
    }
}

// Procedure d'affichage du parcours entre deux sommets //
void Graphe::afficher_parcours(int depart,int arrive){
    
    cout<<"Sommet de Depart :"<< sommets[depart].depart.location.line <<" "<<
                                     sommets[depart].depart.location.column <<" "<<
                                    (int)sommets[depart].depart.status<<endl;

    cout<<"Sommet d'Arrivé :"<< sommets[arrive].depart.location.line <<" "<<
                                     sommets[arrive].depart.location.column <<" "<<
                                     (int)sommets[arrive].depart.status<<endl;

    Sommet * a=&(sommets[arrive]);
    Sommet * d=&(sommets[depart]);
    vector<Sommet*> parcours;

    cout<<"Le parcours se fait en "<< a->distance<<" coups!"<<endl;

    while(a!=nullptr){
        parcours.push_back(a);
        a=a->precedent;
    }
    
    while(d!=a && parcours.size()>0){
        Sommet * s=parcours.back();
        parcours.pop_back();
        Sommet * e=parcours.back();
            for(size_t i=0; i<parcours.size(); i++){
                for(int j=0; j<7; j++){
                    if(d->egal_Robot_Sommet(s) && d->voisins[j].sommetA->egal_Robot_Sommet(e)){
                        cout<<"Mouvement:";
                        d->voisins[j].sommetA->afficher_mouvement(j);
                        cout<<"Direction:";
                        d->voisins[j].sommetA->afficher_direction();
                        d=e;  
                    }
                }

            }
        }
}
        



 void Graphe::parcours_djikstra(int d, int a ){

    Sommet * arrive=&sommets[a];
    Sommet * depart=&sommets[d];
    queue<Sommet*> aTraiter;
    
    for(std::size_t  i=0; i< sommets.size(); i++){
        sommets[i].distance=sommets.size();
        sommets[i].precedent=&(sommets[i]);

        for(int j=0; j<7; j++){
           sommets[i].voisins[j].poids=1;
           sommets[i].mouvements.push_back(sommets[i].voisins[j].move);
          
        } 
    }

    depart->distance=0;
    depart->precedent=nullptr;
    aTraiter.push(depart);
    Sommet * aVisiter;

    while(!aTraiter.empty() && aVisiter!=arrive){
        aVisiter = aTraiter.front(); // on récupère un sommet à traiter
         for(int i=0; i<7; i++){
             if(aVisiter->voisins[i].sommetA->distance > aVisiter->distance + aVisiter->voisins[i].poids ) // +1 car ici les arêtes ont un poid de 1
             {
                  aVisiter->voisins[i].sommetA->distance=aVisiter->distance+ aVisiter->voisins[i].poids;
                  aVisiter->voisins[i].sommetA->precedent=aVisiter;  
                  aTraiter.push(aVisiter->voisins[i].sommetA);
                  
             }
           }
         aTraiter.pop();  //enlever le premier élément qui est aVisite  

    }

 }
 
// Fonctions et Procedures membres de la structure Sommet //

 // Procedure d'affichage de la direction (status) d'un robot //
void Sommet::afficher_direction(){
    if(depart.status==RR::Robot::Status::EAST){
        cout<<"Est"<<endl;
    }
    else if(depart.status==RR::Robot::Status::NORTH){
        cout<<"Nord"<<endl;
    }
    else if(depart.status==RR::Robot::Status::WEST){
        cout<<"Ouest"<<endl;
    }
    else if(depart.status==RR::Robot::Status::SOUTH){
        cout<<"Sud"<<endl;
    }
    else if(depart.status==RR::Robot::Status::DEAD){
        cout<<"Mort"<<endl;
    }
    else cout<<"Problème dans l'affichage de la direction"<<endl;

}
// Procedure d'affichage d'un mouvement //
void Sommet::afficher_mouvement(int i){    
         switch(i){
            case 0: //RR::Robot::Move::FORWARD_1
            cout<<"avancer de 1"<<endl;
            break;
            case 1: //RR::Robot::Move::FORWARD_2
            cout<<"avancer de 2"<<endl;
            break;
            case 2://RR::Robot::Move::FORWARD_3
            cout<<"avancer de 3"<<endl;
            break;
            case 3: //RR::Robot::Move::BACKWARD_1
            cout<<"reculer de 1"<<endl;
            break;
           case 4: //RR::Robot::Move::TURN_LEFT
            cout<<"tourner à gauche"<<endl;
            break;
            case 5: //RR::Robot::Move::TURN_RIGHT
            cout<<"tourner à droite"<<endl;
            break;
            case 6: //RR::Robot::Move::U_TURN
            cout<<" demi-tour"<<endl;
            break;
            default:
             cout<<"Problème dans l'affichage du mouvement"<<endl;
        }        
    
}
// Operateur == pour deux robots //
bool Sommet::egal_Robot_Sommet(Sommet*  s){

    if(depart.status==s->depart.status && depart.location.line==s->depart.location.line && depart.location.column==s->depart.location.column) return true;
    else return false;

}
