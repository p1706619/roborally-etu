#ifndef ROBORALLY_GRAPH_HPP_
#define ROBORALLY_GRAPH_HPP_

#include <iostream>
#include <vector>

#include "board.hpp"

using namespace std;
using namespace RR;



/* Sommet */
struct Sommet {
  
            /* Arêtes */
          struct Arete {
          Sommet * sommetA;
          int poids;
          int move;
          };


  Robot depart;
  Sommet * precedent;
  Arete voisins[7];
  bool visite;
  int distance;
  vector<int> mouvements;
  

  void afficher_direction();// affiche la direction du robot de départ ex:EAST
  void afficher_mouvement(int);// affiche le coup joué par le robot de depart
  bool egal_Robot_Sommet(Sommet*);

};

/* Graphe  */
struct Graphe {
  
  vector<Sommet> sommets;
  
  /* fonctions */

  void init_graphe(Board&); // initialisation du graphe
  void parcourir_graphe(int); // parcours du graphe
  void parcours_djikstra(int,int); // rechercher un chemin
  void verif_parcours(); //verifie si tous les sommets ont été visité
  void afficher_parcours(int,int);
  
  
};



#endif