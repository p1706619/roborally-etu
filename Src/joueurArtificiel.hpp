#ifndef ROBORALLY_JOUEURA_HPP_
#define ROBORALLY_JOUEURA_HPP_

#include <iostream>
#include "graphe.hpp"
#include "board.hpp"


struct JoueurA{

    Sommet * joueur;
    int actions[9];
    int dernierePosition;
    


    void init_tab_actions();
    void echange(int);
    void calcul_chemin(Graphe&,int,int);   

    void afficher_tab_actions(); 
    void afficher_parcours(Graphe*,int,int);

};


#endif