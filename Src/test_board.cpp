#include "board.hpp"
#include "graphe.hpp"
#include "joueurArtificiel.hpp"

#include <iostream>
#include <sstream>

/*int main() {
  RR::Board b("board.txt") ;
  b.save("/tmp/cpy.txt") ;

  return 0 ;
}
*/

int main(){
  RR::Board b("board.txt") ;
  b.save("/tmp/cpy.txt") ;
  
  Graphe graphe;
  int depart=1;
  int arrive=82;
  JoueurA j;
 

  graphe.init_graphe(b);
  j.init_tab_actions();
  
  //graphe.parcourir_graphe(depart);
  graphe.parcours_djikstra(depart,arrive);

  //graphe.afficher_parcours(depart,arrive);

  j.afficher_tab_actions();
  j.calcul_chemin(graphe,depart,arrive);
  
  j.afficher_parcours(&graphe,depart,arrive);
  //j.afficher_tab_actions();

 

  
}









