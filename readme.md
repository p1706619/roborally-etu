 - # Projet Roborally

Fatmana Altay
Nawresse Achour

Ce projet vise à créer un joueur automatique pour le jeu [Roborally](https://fr.wikipedia.org/wiki/RoboRally). 
Le sujet est disponible en pdf [sur la page du cours](https://liris.cnrs.fr/vincent.nivoliers/lifap6/Supports/Projet/roborally.pdf).


Vous trouverez ici notre Projet qui vise a créer un joueur automatique pour le jeu Roborally.
Roborally est une course de robot dont le but est d'être le premier à terminer la course relais sur un plateau reconfigurable à l'infini.
Notre projet est composé d'un sous-repertoire nommé  Src dans lequel il y avait de base les fichiers suivants :
	
- big_board.txt : contient la  configuration d'un grand plateau.

- all_titles.txt : contient une autre configuration des cases.

- board.txt : correspond à la configuration du plateau par default.

- app.cpp : contient un code pour charger un plateau de jeu.

- board.cpp : contient le code pour créer un robot et jouer des coups.

- board.hpp : contient le code de l'entete de board.cpp.

-  test_board.cpp: contient la fonction principale  permettant de tester les différentes fonctions
	
	
Nous y avons ajouté les fichiers:
	

- graphe.cpp : contient  l'implémentation du  code permettant de modeliser un graphe qui pour chaques cases du plateau et chaques orientations du robot créer des  sommets et les relier entre eux, puis  parcours le graphe le but ici est dans un premier temps de trouver un chemin entre un point de départ et un point d'arrivé puis de faire en sorte que ce chemin soit le plus court possible notamment avec dijskra.
- graphe.hpp : contient  l'implémentation d'une structure sommet dans laquelle ont stocke toutes les données relatives à  un sommet et une structure graphe contenant un tableau dynamique de sommets.
- joueurArtificiel.cpp : contient l'implémentation d'un  joueur Artificiel.
- joueurArtificiel.hpp : contient une structure nommé JoueurA dans laquelle on stocke les données caractérisant un joueur artificiel.




Nous avons choisie de stocker nos données relatives à un sommet dans structure contenant :

- Une structure Arete contenant: 
> - un pointeur vers un sommet 
> - un entier représentant le poids
> - un entier représentant les mouvements


- Une structure Sommet contenant: 
> - un Robot qui correspond à  notre point de départ
> - un pointeur vers un sommet qui désigne le sommet précedent
> - un tableau de sept aretes qui constitue les sommets voisins
> - un entier représentant la distance entre deux sommets
> - un vector d'entiers dans lequel nous stokons les mouvements

Puis nous avons également stocké les données caractérisant un graphe dans une structure comportant :
- Une structure Graphe contenant:
> - un vector de Sommet dans lequel nous conservons tout les sommets de notre graphe.
> - une Procedure nommé init_graphe qui étant donnée un plateau  permet d'initialiser un graphe.**La complexité de cette algorithme est au mieux  linéaire** sur la taille du vector de sommets, et au pire de l'ordre de n (n étant la taille du vector) carré.
> - une Procédure nommé parcourir_graphe qui étant donnée un entier representant l'indice du sommet dans le vector de sommet parcours le graphe en largeur. **La complexité de cette procedure est en moyenne (et au pire) cas linéaire** sur la taille du tableau, et au mieux de l'orde du temps constant (pour le sommet contenant le robot mort).
> - une Procédure nommé Afficher_parcours qui étant données deux entiers représentant l'indince du sommet de  départ dans le vector de sommets et pour l'autre l'indice du sommet d'arrivé, affiche le parcours entre ces deux sommets.**La complexité de cette procédure est linéaire sur la taille du parcours entre ces deux sommets.**
> - une Procédure nommé parcours_djikstra qui étant deux entiers représentant l'indince du sommet de  départ dans le vector de sommets et pour l'autre l'indice du sommet d'arrivé, parcours le graphe afin de donner le chemin le plus court possible entre ces deux sommets. **La compléxité de cette procédure est linéaire** sur la taille du vector de sommets.

Enfin nous avons stocké les informations relatives au joueur artificiel dans une structure ayant :
- Une structure JoueurA contenant:
> - un pointeur vers un Sommet qui représente un joueur à l'état actuel.
> - un tableau statique de 9 entiers pour y conservé les actions possibles.
> - un entier représentant la dernièrePosition du joueur dans le graphe de jeu.
> - une Procédure nommé init_tab_actions qui permet d'initialiser le tableau de mouvement et l'indice où on peut commencer à piocher, dont la **complexité est linéaire sur la taille** du tableau.
> - une Procédure nommé afficher_tab_actions permettant d'afficher le tableau de mouvement, dont la **compléxité est linéaire sur la taille du tableau**.
> - une Procédure nommé echange qui echange deux éléments du tableau, celui passé en paramètre et celui en position "dernièrePosition", de **complexité linéaire** sur la taille du tableau.
> - une Procédure chemin qui étant donné un graphe, un entier représentant la position de  depart dans le graphe et un représentant la position d'arrivé nous indique le chemin le plus court possible, de 5 mouvements maximum avec les mouvements du tableau d'actions. Cette algorithme à la même compléxité que l'algorithme de Dijskra mentionné plus haut, c'est à dire **une compléxité linéaire**.

		 

	






